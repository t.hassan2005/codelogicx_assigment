<?php
class validateSudoku{
    var $myArr = array();
    function getInput($input)
    {
        $this->myArr = $input;
    }
     
    function check()
    {
        $bitMap;
        //check all rows for duplicate values
        for($raw = 0 ; $raw <9 ; $raw++){
            $bitMap = 0;            
            for($col = 0 ; $col <9 ; $col++){
                if('0' != $this->myArr[$raw][$col]){        
                    // calculate mask to get or set any particular bit from bitMap
                    $mask = pow(2,$this->myArr[$raw][$col]);                    
                     
                    // check if a bit againt the value is already set to one, 
                    if(($bitMap & $mask) == 0)
                    {
                        $bitMap = $bitMap | $mask;
                    }
                    else
                    {
                        return false;                       
                    } 
                }
            }
        }       
         
        // check all columns for duplicate values
        for($col = 0 ; $col <9 ; $col++){
            $bitMap = 0;
            for($raw = 0 ; $raw <9 ; $raw++){
                if('0' != $this->myArr[$raw][$col]){    
                    // calculate mask to get or set any particular bit from bitMap
                    $mask = pow(2,$this->myArr[$raw][$col]);
                     
                    // check if a bit againt the value is already set to one
                    if(($bitMap & $mask) == 0)
                        $bitMap = $bitMap | $mask;
                    else
                        return false;
                }
            }
        }
         
        $xStart = 0;
        $yStart = 0;
         
        // check for 9 blocks
        for($b = 0 ; $b < 9 ; $b++){
            // set start x index and y index for every block according to block number
            $xStart = floor($b /3) * 3;
            $yStart = ($b % 3) * 3;
            $bitMap = 0;            
            for($x = $xStart ; $x < $xStart + 3 ; $x++){
                for($y = $yStart ; $y < $yStart + 3 ; $y++){ 
                    if('0' != $this->myArr[$x][$y]){
                        $mask = pow(2,$this->myArr[$x][$y]);
                        if(($bitMap & $mask) == 0)
                            $bitMap = $bitMap | $mask;
                        else
                            return false;
                    }
                }
            }
        }
         
        return true;
    }
}
  
$obj = new validateSudoku();
/* $input1 = <<<FOO
132579468
498261375
487964213
756384219
643158792
521793846
987426531
214935687
365817924
FOO; */
$input1 = <<<FOO
132579468
498261375
756384219
643158792
521793846
987426531
214935687
365817924
879642153
FOO; 
$result = explode("\r\n", $input1);
$json = json_encode($result);
$decode = json_decode($json);
$expdata = [];
foreach($decode as $data){
    $expdata[]= ($data);
}

$input = $expdata; 
$obj->getInput($input);
// validate
$response = $obj->check();
if($response){
    echo "Valid";
}else{
    echo "In-valid";
}
 
?>