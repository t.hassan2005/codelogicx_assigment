<?php
$n = 2;
function rotate90Clockwise(&$a)
{
	global $n;
	for ($i = 0; $i < $n / 2; $i++)
	{
		for ($j = $i; $j < $n - $i - 1; $j++)
		{
			$temp                        = $a[$i][$j];
			$a[$i][$j]                   = $a[$n - 1 - $j][$i];
			$a[$n - 1 - $j][$i]          = $a[$n - 1 - $i][$n - 1 - $j];
			$a[$n - 1 - $i][$n - 1 - $j] = 	$a[$j][$n - 1 - $i];
			$a[$j][$n - 1 - $i]          = $temp;
		}
	}
}

// Function for print matrix
function printMatrix($arr)
{
	global $n;
	for ($i = 0; $i < $n; $i++)
	{
		for ($j = 0; $j < $n; $j++)
			echo $arr[$i][$j] . " ";
		echo "\n";
	}
    //print_r($arr);
}

// INPUT
$arr = [
    [1, 2],
    [3, 4]
];

rotate90Clockwise($arr);
printMatrix($arr);
?>
